import React from 'react';
import axios from 'axios';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';

import './Blog.css';

class Blog extends React.Component {
  state = {
    posts: [],
    selectPostId: null,
    error: false,
  };

  componentDidMount() {
    axios
      .get('/posts')
      .then((res) => {
        const posts = res.data.slice(0, 4);
        const updatedPosts = posts.map((item) => {
          return {
            ...item,
            author: 'Soheyl',
          };
        });
        this.setState({ posts: updatedPosts });
      })
      .catch((err) => this.setState({ error: true }));
  }

  selectPostHandler = (id) => {
    this.setState({ selectPostId: id });
  };

  render() {
    let post = <p style={{ textAlign: 'center' }}>Error Fetching Failed</p>;
    if (!this.state.error) {
      post = this.state.posts.map((item) => {
        return (
          <Post
            key={item.id}
            title={item.title}
            author={item.author}
            click={() => this.selectPostHandler(item.id)}
          />
        );
      });
    }

    return (
      <div>
        <section className="posts">{post}</section>
        <section>
          <FullPost id={this.state.selectPostId} />
        </section>
        <section>
          <NewPost />
        </section>
      </div>
    );
  }
}

export default Blog;
